import React, {Component} from 'react';

import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            gender: 0,
            number: 0,
            contestants: [],
            contReady: false,
            valueInput: 0.0,
            dayActivation: 1
        };
    }

    pointCount(number, a, b, c, type, event) {

        event.preventDefault();

        let unit = event.target.unitInput.value;
        let points = 0;

        if (type === "run") {
            points = parseInt((a * (Math.pow(b - unit, c))));
        } else if (type === "jump" || type === "throw") {
            points = parseInt((a * (Math.pow(unit - b, c))));
        }

        if (unit !== "") {
            if (event.target.resetBtn.style.display === "none") {
                event.target.resetBtn.style.display = "initial";
                event.target.setBtn.style.display = "none";
                event.target.unitInput.disabled = "disabled";
                points = this.state.contestants[number].points + points;
            } else {
                event.target.resetBtn.style.display = "none";
                event.target.setBtn.style.display = "initial";
                event.target.unitInput.disabled = "";
                points = this.state.contestants[number].points - points;
            }
            if (isNaN(points)){
                points = 0;
            }
            this.state.contestants[number].points = points;
            this.forceUpdate();
        }
    }

    static setGender(e) {
        this.setState({
            gender: parseInt(e.target.value)
        });

        if (this.state.gender === 0) {
            this.setState({
                number: 0,
                contestants: [],
                contReady: false,
                dayActivation: 1
            });
        }
    }

    static setContestantNumber(e) {
        this.setState({
            number: parseInt(e.target.value)
        });
        // console.log(this.state.number);
    }

    setContestants() {

        let contNumb = parseInt(this.state.number);
        let contestants = [];

        for (let i = 0; i < contNumb; i++) {
            let tempCont = {
                number: i + 1,
                name: "",
                points: 0
            };
            contestants = contestants.concat(tempCont);
        }
        this.setState({
            contestants: contestants,
            contReady: true
        });
    }

    setDay(e) {
        if (this.state.dayActivation === 1) {
            this.setState({
                dayActivation: 2
            });
            e.target.innerHTML = "<--";
        } else if (this.state.dayActivation === 2) {
            this.setState({
                dayActivation: 1
            });
            e.target.innerHTML = "-->";

        }
    }

    componentDidMount() {
    }

    render() {

        const {
            gender,
            contestants,
            dayActivation
        } = this.state;

        const contListDay = contestants.map((cont, i) => {
            return <div key={i} className="personDiv">
                <b>#{cont.number} </b>{/*<input type={"text"} placeholder="Name"/>*/}<br/>
                {gender === 1 ? (
                    <div key={i}>
                        <div style={{display: dayActivation === 1 ? 'inline-block' : 'none'}}>
                            <ul>
                                <li><b>100 Meter Run</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 25.4347, 18, 1.81, "run", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Seconds" disabled=""/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>Long Jump</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 0.14354, 220, 1.40, "jump", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Cm"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>Shot Put</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 51.39, 1.50, 1.05, "throw", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Meters"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>High Jump</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 0.8465, 75.0, 1.42, "jump", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Cm"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>400 Meter Run</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 1.53775, 82, 1.81, "run", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Seconds"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                        <div style={{display: dayActivation === 2 ? 'inline-block' : 'none'}}>
                            <ul>
                                <li><b>110 Meter Hurdles</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 5.74352, 28.5, 1.92, "run", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Seconds"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>Discus Throw</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 12.91, 4.0, 1.1, "throw", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Meters"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>Pole vault</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 0.2797, 100, 1.35, "jump", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Cm"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>Javelin throw</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 10.14, 7.0, 1.08, "throw", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Meters"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>1500 Meter Run</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 0.03768, 480, 1.85, "run", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Seconds"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>) : (null)}
                {gender === 2 ? (
                    <div key={i}>
                        <div style={{display: dayActivation === 1 ? 'inline-block' : 'none'}}>
                            <ul>
                                <li><b>100 Meter Run</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 25.4347, 18, 1.81, "run", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Seconds"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"removeBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>Discus Throw</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 12.91, 4.0, 1.1, "throw", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Meters"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>Pole vault</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 0.2797, 100, 1.35, "jump", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Cm"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>Javelin throw</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 10.14, 7.0, 1.08, "throw", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Meters"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>400 Meter Run</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 1.53775, 82, 1.81, "run", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Seconds"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                        <div style={{display: dayActivation === 2 ? 'inline-block' : 'none'}}>
                            <ul>
                                <li><b>110 Meter Hurdles</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 5.74352, 28.5, 1.92, "run", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Seconds"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>Long Jump</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 0.14354, 220, 1.40, "jump", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Cm"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>Shot Put</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 51.39, 1.50, 1.05, "throw", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Meters"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>High Jump</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 0.8465, 75.0, 1.42, "jump", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Cm"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                                <li><b>1500 Meter Run</b><br/>
                                    <form onSubmit={(e) => this.pointCount(i, 0.03768, 480, 1.85, "run", e)}>
                                        <input className={"unitInput"} name={"unitInput"}
                                               type="number" step="0.01" placeholder="Seconds"/>
                                        <button name={"setBtn"}>Set</button>
                                        <button style={{display: "none"}} name={"resetBtn"}>Reset</button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                ) : (null)}
            </div>
        });

        let tempContestants2 = [...contestants].sort((a, b) => b.points - a.points);

        const scoreboard = (
            <table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Points</th>
                </tr>
                </thead>
                {tempContestants2.map((cont, i) => {
                    return <tr key={i}>
                        <td>{cont.number}</td>
                        <td>{cont.points}</td>
                    </tr>
                })}
            </table>
        );


        return (
            <div className="App fade-in">
                <select onChange={App.setGender.bind(this)} className={"genderSelect"}>
                    <option value={0}>Competing gender:</option>
                    <option value={1}>Men</option>
                    <option value={2}>Women</option>
                </select>
                {this.state.gender > 0 ? (
                    <div className={"contestDiv fade-in"}>
                        <div className={"pointCalDiv fade-in"}>
                            <input onChange={App.setContestantNumber.bind(this)}
                                   className={"contNumb"} type="number" placeholder="Number of contestants"/>
                            <button onClick={() => this.setContestants()}>Set</button>
                            <div>
                                <div>
                                    <h2>{dayActivation === 1 ? 'Day 1' : 'Day 2'}</h2>
                                    <button className={"dayBtn"} onClick={(e) => this.setDay(e)}>--></button>
                                    <br/>
                                </div>
                                {contListDay}
                            </div>
                        </div>
                        <div className={"scoreDiv fade-in"}>
                            <b>Scoreboard</b>
                            {scoreboard}
                        </div>
                    </div>
                ) : (null)}
            </div>
        );
    }
}

export default App;
